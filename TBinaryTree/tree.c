#include "tree.h"
#include "stdlib.h"
#include "comparable.h"

typedef struct node TNode;

typedef struct node{
  void *data;
  TNode *left;
  TNode *right;
  TNode *father;
}TNode;

typedef struct{
  TNode *root;
  int length;
}TBinaryTreeData;

static int af_max(int a, int b){
  return a > b? a : b;
}

static int af_length(TNode* tree){
  if(tree != NULL){
    return (af_max(af_length(tree->left), af_length(tree->right)))+1;
  }else{
    return -1;
  }
}

static int af_empty(TNode* tree){
  return tree == NULL;
}

static TNode* af_createNode(TNode *father,void *info){
  TNode *newNode = calloc(1,sizeof(TNode));
  newNode->father = father;
  newNode->data = info;
  return newNode;
}

static int af_searchNode(TNode *father, void *info){
  if(af_empty(father)){
    return 0;
  }else{
    IComparable *comparable = father->data;
    return comparable->compare(comparable,info) == 0 || af_searchNode(father->left, info) || af_searchNode(father->right, info);
  }
}

static TNode* af_insertNode(TNode *father, IComparable *comparable){
  if(father == NULL){
    return af_createNode(father, comparable);
  }else{
    if(comparable->compare(comparable,father->data)<0){
      father->left = af_insertNode(father->left,comparable);
    }else{
      father->right = af_insertNode(father->left,comparable);
    }
    return father;
  }
}

static void af_deleteNode(TNode *father, IComparable *info){
}

static void f_insertNode(TBinaryTree *tree, void *info){
  TBinaryTreeData *treeData = tree->data;
  af_insertNode(treeData->root,info);
}

static int f_searchNode(TBinaryTree *tree, void *info){
  TBinaryTreeData *treeData = tree->data;
  return af_searchNode(treeData->root,info);
}

static void f_deleteNode(TBinaryTree *tree, void *info){
  TBinaryTreeData *treeData = tree->data;
  af_deleteNode(treeData->root,info);
}

TBinaryTree *new_btree(){
  TBinaryTree *tree = malloc(sizeof(TBinaryTree));
  TBinaryTreeData *treeData = malloc(sizeof(TBinaryTreeData));
  tree->data = treeData;

  treeData->length = 0;
  treeData->root = NULL;

  tree->search = f_searchNode;
  tree->insert = f_insertNode;
  tree->delete = f_deleteNode;
  return tree;
}
