typedef struct binarytree TBinaryTree;

typedef void(*FInsertItemBinaryTree)(TBinaryTree*,void*);
typedef int(*FSearchItemBinaryTree)(TBinaryTree*, void*);
typedef void(*FDeleteItemBinaryTree)(TBinaryTree*, void*);

typedef struct binarytree{
  void *data;
  FInsertItemBinaryTree insert;
  FSearchItemBinaryTree search;
  FDeleteItemBinaryTree delete;
}TBinaryTree;


TBinaryTree* new_btree();
