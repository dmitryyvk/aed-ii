typedef struct comparable IComparable;

typedef short(*FCompare)(void*,void*);

typedef struct comparable{
  void *data;
  FCompare compare;
}IComparable;
