typedef struct dArray TDArray;

typedef void* (*FGetDArrayItem)(TDArray*,int);
typedef void (*FSetDArrayItem)(TDArray*,void*,int);
typedef int (*FGetDArraySize)(TDArray*);


 struct dArray{
  void *data;
  FGetDArrayItem getDArrayItem;
  FSetDArrayItem setDArrayItem;
  FGetDArraySize getDArraySize;
};

TDArray *new_darray();
