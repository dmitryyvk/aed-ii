#include "array.h"
#include "stdlib.h"
#include "math.h"

typedef struct{
  void **vector;
  int size;
}TDArrayData;

static void af_resizeVector(TDArray* array, int referential){
  TDArrayData *data = array->data;
  int k = floor(log2(referential)) + 1;
  int newSize = (int)pow(2,k);
  void **newVector = calloc(newSize,sizeof(void*));

  for(int i=0;i<data->size;i++){
    newVector[i] = data->vector[i];
  }

  free(data->vector);
  data->vector = newVector;
  data->size = newSize;

}

static void f_seTDArrayItem(TDArray* array, void* item, int pos){
  TDArrayData *data = array->data;

  if(data->size <= pos){
    af_resizeVector(array, pos);
  }

  data->vector[pos] = item;

}

static void *f_geTDArrayItem(TDArray* array, int pos){
  TDArrayData *data = array->data;
  if(pos < data->size){
    return data->vector[pos];
  }else{
    return NULL;
  }
}

static int f_arraySize(TDArray* array){
  return ((TDArrayData*)array->data)->size;
}

TDArray *new_darray(){
  TDArray *arr = malloc(sizeof(TDArray));
  TDArrayData *data = malloc(sizeof(TDArrayData));
  arr->data = data;
  data->size = 2;
  data->vector = calloc(data->size,sizeof(void*));

  arr->getDArrayItem = f_geTDArrayItem;
  arr->getDArraySize = f_arraySize;
  arr->setDArrayItem = f_seTDArrayItem;

  return arr;
}
