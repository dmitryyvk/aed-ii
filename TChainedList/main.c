#include "doge.h"
#include "stdio.h"
#include "stdlib.h"
#include "chainedlist.h"

int *getPointer(int a){
  int *b = malloc(sizeof(int));
  *b = a;
  return b;
}

int main(){
  TChainedList *list = new_chainedList();
  list->chainItem(list,new_doge(0, "Rurik", 1));
  list->chainItem(list,new_doge(1, "Pakko", 2));

  list->printItems(list);
  list->deleteItem(list,getPointer(0));
  list->printItems(list);
  return 0;
}
