#include "doge.h"
#include "stdlib.h"
#include "stdio.h"

typedef struct{
  int code;
  int age;
  char* name;
}TDogeData;

static void f_printDoge(TDoge *doge){
  TDogeData *data = doge->data;
  printf("There is a doge called %s w/ %d yo",data->name,data->age);
}

static short f_compareDoge(TDoge* a, TDoge *b){
  TDogeData *dataA = a->data;
  TDogeData *dataB = b->data;
  return dataA->age == dataB->age? 0 : dataA->age < dataB->age ? -1 : 1;
}

static short f_compareDogeKey(TDoge *doge, int* code){
  TDogeData *data = doge->data;
  return data->code == *code? 0 : data->code < *code ? -1 : 1;
}

static char* f_getDogeName(TDoge *doge){
  TDogeData *data = doge->data;
  return data->name;
}

static int f_getDogeAge(TDoge *doge){
  TDogeData *data = doge->data;
  return data->age;
}

static int f_getDogeKey(TDoge *doge){
  TDogeData *data = doge->data;
  return data->code;
}


TDoge *new_doge(int code ,char* name, int age){
  TDoge *doge = malloc(sizeof(TDoge));
  doge->chainableInstance = malloc(sizeof(TChainable));
  TDogeData *dogeData = malloc(sizeof(TDogeData));
  doge->data = dogeData;
  dogeData->code = code;
  dogeData->age = age;
  dogeData->name = name;

  doge->getAge = f_getDogeAge;
  doge->getName = f_getDogeName;
  doge->getKey = f_getDogeKey;

  doge->chainableInstance->print = (FPrintChainable)f_printDoge;
  doge->chainableInstance->compare = (FCompareChainable)f_compareDoge;
  doge->chainableInstance->compareKey = (FCompareKeyChainable)f_compareDogeKey;

  return doge;
}
