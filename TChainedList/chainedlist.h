typedef struct chainedList TChainedList;

typedef void (*TChainChainedItem)(TChainedList*, void*);
typedef void* (*TUnchainChainedItem)(TChainedList*, void*);
typedef void (*TDeleteChainedItem)(TChainedList*, void*);
typedef void (*TPrintChainedItems)(TChainedList*);

typedef struct chainedList{
  void* data;
  TChainChainedItem chainItem;
  TUnchainChainedItem unchainItem;
  TDeleteChainedItem deleteItem;
  TPrintChainedItems printItems;
}TChainedList;


TChainedList *new_chainedList();
