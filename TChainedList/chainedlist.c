#include "chainedlist.h"
#include "stdlib.h"
#include "chainable.h"

typedef struct chainedItem TChainedItem;

typedef struct chainedItem{
  TChainedItem* next;
  void* content;
}TChainedItem;

typedef struct{
  TChainedItem* first;
  int breadth;
}TChainedListData;

static void f_printItems(TChainedList *list){
  TChainedListData *data = list->data;
  TChainedItem *item = data->first;
  IChainable *chainable;

  while(item){
    chainable = item->content;
    chainable->chainableInstance->print(chainable);
    item = item->next;
  }
}

static TChainedItem *af_createItem(void* content){
  TChainedItem *item = calloc(1,sizeof(TChainedItem));
  item->content = content;
  return item;
}

static void f_chainItem(TChainedList *list, void* item){
  TChainedListData *data = list->data;
  TChainedItem *newoItem = af_createItem(item);

  newoItem->next = data->first;
  data->first = newoItem;
  ++data->breadth;
}

static void* f_unchainItem(TChainedList* list, void *key){
  TChainedListData *data = list->data;
  TChainedItem *lastIterator = NULL;
  TChainedItem *iterator = data->first;
  IChainable * chainable = NULL;
  void *content = NULL;


  /* A IDÉIA É PASSAR COMO PARÂMETRO UM PONTEIRO DE UM TAD E UMA CHAVE PARA COMPARAÇÃO, EXEMPLO :

  static short comparaChave(TDoge *doge, int* age){
  TDogeData *data = doge->data;
  return dataA->age == *age? 0 : dataA->age < *age ? -1 : 1;
}

*/
while(iterator){
  chainable = iterator->content;
  if(chainable->chainableInstance->compareKey(chainable,key) == 0){
    lastIterator->next = iterator->next;
    content = iterator->content;
    free(iterator);
    return content;
  }else{
    lastIterator = iterator;
    iterator=iterator->next;
  }
}
return NULL;
}

static void f_deleteItem(TChainedList* list, void *key){
  void *toDelete = f_unchainItem(list,key);
  if(toDelete){
    free(toDelete);
  }
}


TChainedList *new_chainedList(){
  TChainedList *list = calloc(1,sizeof(TChainedList));
  TChainedListData *data = calloc(1,sizeof(TChainedListData));
  list->data = data;
  data->first = NULL;
  data->breadth = 0;

  list->printItems = f_printItems;
  list->chainItem = f_chainItem;
  list->unchainItem = f_unchainItem;
  list->deleteItem = f_deleteItem;

  return list;
}
