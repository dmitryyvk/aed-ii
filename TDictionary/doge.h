#include "chainable.h"

typedef struct doge TDoge;
typedef char*(*FGetDogeName)(TDoge*);
typedef int(*FGetDogeAge)(TDoge*);
typedef int(*FGetDogeKey)(TDoge*);

typedef struct doge{
  void *data;
  TChainable *chainableInstance;
  FGetDogeName getName;
  FGetDogeAge getAge;
  FGetDogeKey getKey;
}TDoge;


TDoge *new_doge(int,char*,int);
