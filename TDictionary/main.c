#include "dictionary.h"
#include "doge.h"
#include "stdio.h"

int main(){
  TDictionary *dictionary = new_dictionary(10, 0.8f);
  IChainable *chain;
  TDoge *doge1 = new_doge(0, "Rurik", 1);
  TDoge *doge2 = new_doge(2, "Pakko", 2);
  TDoge *doge3 = new_doge(9, "Carzalbe", 3);
  TDoge *doge4 = new_doge(3, "Digital", 2);
  TDoge *doge5 = new_doge(1, "Naozao", 6);
  TDoge *doge6 = new_doge(4, "Sloth", 3);
  TDoge *doge7 = new_doge(7, "Etapoarr", 1);
  TDoge *doge8 = new_doge(8, "Trycatch", 2);
  TDoge *doge9 = new_doge(11, "Kalashnikov", 1);
  TDoge *doge10 = new_doge(10, "Glasnost", 3);
  TDoge *doge11 = new_doge(6, "Perestroika", 1);
  TDoge *doge12 = new_doge(5, "Russinho", 6);
  dictionary->insert(dictionary,doge1,doge1->getKey(doge1));
  dictionary->insert(dictionary,doge2,doge2->getKey(doge2));
  dictionary->insert(dictionary,doge3,doge3->getKey(doge3));
  dictionary->insert(dictionary,doge4,doge4->getKey(doge4));
  dictionary->insert(dictionary,doge5,doge5->getKey(doge5));
  dictionary->insert(dictionary,doge6,doge6->getKey(doge6));
  dictionary->insert(dictionary,doge7,doge7->getKey(doge7));
  dictionary->insert(dictionary,doge8,doge8->getKey(doge8));
  dictionary->insert(dictionary,doge9,doge9->getKey(doge9));
  dictionary->insert(dictionary,doge10,doge10->getKey(doge10));
  dictionary->insert(dictionary,doge11,doge11->getKey(doge11));
  dictionary->insert(dictionary,doge12,doge12->getKey(doge12));
  dictionary->printDictionary(dictionary);
  printf("\nSearch for : ");
  int key;
  scanf("%d",&key);
  printf("\n\nSearching...\n\n");
  chain = dictionary->search(dictionary,key);
  if(chain){
  chain->chainableInstance->print(chain);
}else{
printf("Not found");
}
return 0;
}
