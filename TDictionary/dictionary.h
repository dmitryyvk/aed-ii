typedef struct dictionary TDictionary;

typedef void (*FInsertDictionaryItem)(TDictionary*,void*,int);
typedef void* (*FSearchDictionaryItem)(TDictionary*,int);
typedef void (*FPrintDictionary)(TDictionary*);

typedef struct dictionary{
  void *data;
  FInsertDictionaryItem insert;
  FSearchDictionaryItem search;
  FPrintDictionary printDictionary;
}TDictionary;


TDictionary *new_dictionary(int,float);
