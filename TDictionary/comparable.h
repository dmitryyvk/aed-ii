typedef struct i_comparable IComparable;

typedef short(*TCompare)(void*,void*);

typedef struct i_comparable{
  void *data;
  TCompare compare;
}IComparable;
