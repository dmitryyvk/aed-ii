#include "dictionary.h"
#include "stdlib.h"
#include "math.h"
#include "stdio.h"
#include "chainable.h"

#define throw(x) printf(x)

typedef struct{
  void *data;
  int key;
  int prospections;
}TDictionaryItem;

typedef struct{
  TDictionaryItem **items;
  int length;
  int used;
  float chargeFactor;
}TDictionaryData;

// NECESSARY ASSIGNATURE
static void af_insertDictionaryItem(TDictionary*,TDictionaryItem*);

static TDictionaryItem* af_createItem(void* data, int key){
  TDictionaryItem *item = malloc(sizeof(TDictionaryItem));
  item->data = data;
  item->key = key;
  item->prospections=0;
  return item;
}

static void af_rehash(TDictionary *dictionary){
  TDictionaryData *data = dictionary->data;
  int oldLength = data->length;
  TDictionaryItem **oldVector = data->items;

  data->length = data->length + data->length;
  data->used=0;
  data->items = calloc(data->length,sizeof(TDictionaryItem*));

  for(int i=0;i<oldLength;i++){
    if(oldVector[i]){
      af_insertDictionaryItem(dictionary,oldVector[i]);
    }
  }

  free(oldVector);
}

static int f_hash(TDictionaryData *data,int key){
  return floor(data->length * fmod((key * M_PI+data->length),1.0));
}

static void f_printDictionary(TDictionary * dictionary){
  TDictionaryData *data = dictionary->data;
  IChainable *chainable;
  TDictionaryItem *item;
  printf("\nDictionary $ chargeFactor - %lf / chargeUsed - %lf  ||  length - %d / used - %d",data->chargeFactor,(float)data->used/(float)data->length,data->length,data->used);
  for(int i=0;i<data->length;i++){
    printf("\n");
    if(data->items[i]){
      chainable = data->items[i]->data;
      item = data->items[i];
      printf("{%d - ",i);
      chainable->chainableInstance->print(chainable);
      printf(" | Prospections : %d ; Key : %d ; Hash : %d}",item->prospections,item->key,f_hash(data,item->key));
    }else{
      printf("{%d - NULL}",i);
    }
  }
  printf("\n");
}

static void af_insertDictionaryItem(TDictionary* dictionary, TDictionaryItem *item){
  TDictionaryData *data = dictionary->data;
  int pos = f_hash(data, item->key);
  int walked = 0;
  while(data->items[pos] != NULL){pos=(pos+1) % data->length; walked++;}

  data->items[pos] = item;
  data->items[pos]->prospections = walked;
  data->used++;
}

static void f_insertDictionaryItem(TDictionary *dictionary, void *dataI, int key){
  TDictionaryData *data = dictionary->data;
  if(data->chargeFactor > (float)(data->used+1)/(float)data->length){
    af_insertDictionaryItem(dictionary, af_createItem(dataI, key));
  }else{
    printf("\n[LOG] : Charge factor overloaded! {cf : %.2f, uf : %.2f}",data->chargeFactor,(float)(data->used+1)/(float)data->length);
    af_rehash(dictionary);
    af_insertDictionaryItem(dictionary,af_createItem(dataI, key));
  }
}

static void* f_searchDictionaryItem(TDictionary *dictionary, int key){
  TDictionaryData *data = dictionary->data;
  int pos = f_hash(data,key);
  int walked = 0;

  while(data->items[pos] != NULL && walked < data->length){
    if(data->items[pos]->key == key){
      return data->items[pos]->data;
    }else{
      pos = (pos+1) % data->length;
      walked++;
    }
  }

  return NULL;
}

TDictionary *new_dictionary(int length,float chargeFactor){
  if(chargeFactor > 1.0f){
    throw("ChargeFactor cannot be bigger than 1.0f!");
    return NULL;
  }else{
    TDictionary *newoDictionary = malloc(sizeof(TDictionary));
    TDictionaryData *newoDictionaryData = malloc(sizeof(TDictionaryData));
    newoDictionary->data = newoDictionaryData;

    newoDictionaryData->used = 0;
    newoDictionaryData->length = ceil(((float)length) / chargeFactor);
    newoDictionaryData->chargeFactor = chargeFactor;
    newoDictionaryData->items = calloc(newoDictionaryData->length,sizeof(TDictionaryItem*));

    newoDictionary->insert = f_insertDictionaryItem;
    newoDictionary->search = f_searchDictionaryItem;
    newoDictionary->printDictionary = f_printDictionary;

    return newoDictionary;
  }
}
