typedef struct chainableType TChainable;
typedef struct chainable IChainable;

typedef void(*FPrintChainable)(void*);
typedef short(*FCompareChainable)(void*,void*);
typedef short(*FCompareKeyChainable)(void*,void*);

typedef struct chainableType{
  FPrintChainable print;
  FCompareChainable compare;
  FCompareKeyChainable compareKey;
}TChainable;

typedef struct chainable{
  void *data;
  TChainable *chainableInstance;
}IChainable;
